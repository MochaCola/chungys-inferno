extends KinematicBody2D

var state_machine
var motion = Vector2()
var direction = -1
var health = 2

var stunned = false
var dead = false

onready var hitSFX = get_node("Hitmarker")
onready var dieSFX = get_node("Explosion")

const UP = Vector2(0, -1)
const GRAVITY = 20
const SPEED = 80
const JUMP = -200

func _ready():
	var tree = $skeleton/AnimationPlayer/AnimationTree
	state_machine = tree.get("parameters/playback")

func _physics_process(delta):
	
	# If stunned, keep enemy still on x-axis
	if stunned or dead:
		motion.x = 0
	else:
		state_machine.travel("Walk")
		motion.x = SPEED * direction
		if is_on_floor() == false:
			motion.y += GRAVITY
	move_and_slide(motion, UP)
		
	# Turn around on a wall, or on a ledge
	if is_on_wall() or not $RayCast2D.is_colliding(): turn()
		
	# Turn if on stairs
	if get_floor_normal().y != -1:
		turn()

# Turn function (adjusts all collision)
func turn():
	
	# Timer used so collision boxes won't get stuck in tiles
	direction = direction * -1
	if not dead:
		$Body.position.x *= -1
		$RayCast2D.position.x *= -1
	
	# Sprite offset
	if direction == 1: $skeleton.flip_h = false
	else: $skeleton.flip_h = true
	

# When hit with bolas, stun enemy, decrement health is stomped
func stun():
	stunned = true
	state_machine.travel("Stun")
	$Stun.show()
	$StunTimer.start()

# When dead, play explosion and remove collision
func dead():
	remove_child($Body)
	dieSFX.play()
	$Stun.hide()
	$DeathTimer.start()
	dead = true
	motion = Vector2(0, 0)
	$skeleton.hide()
	$Death.show()
	state_machine.travel("Death")

func _on_StunTimer_timeout():
	stunned = false
	$Stun.hide()

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		if not dead:
			get_parent().get_node("Player").hop()
			if stunned == true:
				hitSFX.play()
				health -= 1
				$skeleton.modulate = Color(1, 0, 0)
				$skeleton.self_modulate = Color(1, 0, 0)
				$HitTimer.start()
			if health == 0: dead()

func _on_HitTimer_timeout():
	$skeleton.set_modulate(Color(0.69, 0.66, 0.66))
	$skeleton.set_self_modulate(Color(1, 1, 1))

func _on_DeathTimer_timeout():
	stunned = false
	queue_free()
