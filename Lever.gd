extends Node2D

var pulled = false

func _ready():
	$AnimatedSprite.frame = 0

func _process(delta):
	if Input.is_action_just_pressed("activate") and pulled == false:
		pulled = true
		get_parent().get_node("Gate").activate()
		$AnimatedSprite.play("Pull")
		$E_Icon.hide()
		$E_Icon_Inactive.show()

func _on_Lever_body_entered(body):
	if body.name == "Player":
		if pulled: $E_Icon_Inactive.show()
		else: $E_Icon.show()

func _on_Lever_body_exited(body):
	if body.name == "Player":
		if pulled: $E_Icon_Inactive.hide()
		else: $E_Icon.hide()
