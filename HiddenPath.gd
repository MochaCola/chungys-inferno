extends Node2D

var fade = false
var modular = 1

func _process(delta):
	if fade == true:
		modular -= 0.01
		if modular < 0:
			fade = false
			removeCollision()
		else: self.modulate = Color(1, 1, 1, modular)

func activate():
	fade = true

func removeCollision():
	for tiles in get_children():
		tiles.clear()
