extends Area2D

const SPEED = 350
var motion = Vector2()
var direction = 1

func set_bolas_direction(dir):
	
	direction = dir
	if dir == -1:
		$flySprite.flip_h = true
	else:
		$flySprite.flip_h = false

func _physics_process(delta):
	motion.x = SPEED * delta * direction
	translate(motion)
	$flySprite.play("fly")

func _on_Bolas_body_entered(body):
	if "Enemy" in body.name:
		body.stun()
	elif "Chungus" in body.name:
		body.stunHit()
	elif "BolasTarget" in body.name:
		body.get_parent().hit()
	get_parent().get_node("Player").cooldown()
	queue_free()
