extends Node2D

func _ready():
	var health_frame = 6 - global.health
	$Player/Health.frame = health_frame
	$Player/ParallaxBackground/ParallaxLayer/background.texture = load("res://Inferno Background.png")
	$Player/ParallaxBackground/ParallaxLayer/background.scale = Vector2(1, 1)
