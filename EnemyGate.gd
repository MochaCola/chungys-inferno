extends Area2D

var opening = false
var gate_pos
var player
var player_pos


func _process(delta):
	if opening == true:
		player.get_node("Health").hide()
		player.get_node("BolasIcon").hide()
		player.position = gate_pos
		player.freeze()

# Activate called by enemy death, store values, updates player camera, waits 0.3 seconds
func activate():
	# The moment the player presses E, store positions
	gate_pos = get_parent().get_node("EnemyGate").position
	player = get_parent().get_node("Player")
	player_pos = player.position
	$EnemyDelay.start()

# After 0.3 seconds, begin GateBottom, wait 0.4 seconds
func _on_EnemyDelay_timeout():
	opening = true
	player.get_node("Sprite").hide()
	$EnemyAnimationTimer.start()
	$EnemyGateBottom.play()
	$EnemySmoothTimer1.start()

# After 0.4 seconds, begin GateMiddle, wait 0.4 seconds
func _on_EnemySmoothTimer1_timeout():
	$EnemyGateMiddle.play()
	$EnemySmoothTimer2.start()
	$StaticBody2D/CollisionShape2D.disabled = true
	$StaticBody2D.remove_child($CollisionShape2D)

# After 0.4 seconds, begin GateTop, wait for AnimationTimer
func _on_EnemySmoothTimer2_timeout():
	$EnemyGateTop.play()

# When AnimationTimer finishes, reset player position & camera
func _on_EnemyAnimationTimer_timeout():
	opening = false
	player.position = player_pos
	player.get_node("Sprite").show()
	player.get_node("Health").show()
	player.get_node("BolasIcon").show()
