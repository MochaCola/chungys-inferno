extends Area2D

var show = false
var fade = 0.0

func _ready():
	for node in get_children():
		node.modulate = Color(1, 1, 1, 0)

func _process(delta):
	if show:
		if fade < 1:
			fade += 0.1
			for node in get_children():
				node.modulate = Color(1, 1, 1, fade)
	elif fade > 0:
		fade -= 0.1
		for node in get_children():
			node.modulate = Color(1, 1, 1, fade)

func _on_Sprite_Set_4_body_entered(body):
	if body.name == "Player":
		show = true


func _on_Sprite_Set_4_body_exited(body):
	if body.name == "Player":
		show = false
