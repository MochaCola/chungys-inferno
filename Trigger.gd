extends Area2D

var player

func _ready():
	player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = load("res://Sounds//bossMusic.wav")

func _on_Trigger_body_entered(body):
	if body.name == "Player":
		player.play()
		body.get_node("ChungusHP").show()
		body.get_node("ChungusStun").show()
	
