extends Area2D

var opening = false
var gate_pos
var player
var player_pos


func _process(delta):
	if opening == true:
		player.get_node("Health").hide()
		player.get_node("BolasIcon").hide()
		player.position = gate_pos
		player.freeze()

# Activate called by lever, store values, updates player camera, waits 0.3 seconds
func activate():
	# The moment the player presses E, store positions
	gate_pos = get_parent().get_node("BolasGate").position
	player = get_parent().get_node("Player")
	player_pos = player.position
	$BolasDelay.start()

# After 0.3 seconds, begin GateBottom, wait 0.4 seconds
func _on_BolasDelay_timeout():
	opening = true
	player.get_node("Sprite").hide()
	$BolasAnimationTimer.start()
	$BolasGateBottom.play()
	$BolasSmoothTimer1.start()

# After 0.4 seconds, begin GateMiddle, wait 0.4 seconds
func _on_BolasSmoothTimer1_timeout():
	$BolasGateMiddle.play()
	$BolasSmoothTimer2.start()
	$StaticBody2D/CollisionShape2D.disabled = true
	$StaticBody2D.remove_child($CollisionShape2D)

# After 0.4 seconds, begin GateTop, wait for AnimationTimer
func _on_BolasSmoothTimer2_timeout():
	$BolasGateTop.play()

# When AnimationTimer finishes, reset player position & camera
func _on_BolasAnimationTimer_timeout():
	opening = false
	player.position = player_pos
	player.get_node("Sprite").show()
	player.get_node("Health").show()
	player.get_node("BolasIcon").show()
