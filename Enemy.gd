extends KinematicBody2D

var state_machine
var motion = Vector2()
var direction = -1
var health = 3

var stunned = false
var dead = false
var can_charge = true
var pre_charge = false
var charge = false

onready var hitSFX = get_node("Hitmarker")
onready var dieSFX = get_node("Explosion")

const UP = Vector2(0, -1)
const GRAVITY = 20
const SPEED = 80
const JUMP = -200
const PRECHARGE = -20
const CHARGE = 300

func _ready():
	var tree = $spr_ape_yeti/AnimationPlayer/AnimationTree
	state_machine = tree.get("parameters/playback")

func _physics_process(delta):
	
	# If stunned, keep enemy still on x-axis
	if pre_charge:
		motion.x = PRECHARGE * direction
	elif charge:
		motion.x = CHARGE * direction
	elif stunned or dead:
		motion.x = 0
	else:
		state_machine.travel("Default")
		motion.x = SPEED * direction
		if is_on_floor() == false:
			motion.y += GRAVITY
	move_and_slide(motion, UP)
		
	# Turn around on a wall, or on a ledge
	if is_on_wall() or not $RayCast2D.is_colliding():
		turn()
		if charge:
			charge = false
			$CooldownTimer.start()
		
	# Turn if on stairs
	if get_floor_normal().y != -1:
		turn()
		charge = false
		$CooldownTimer.start()

# Turn function (adjusts all collision)
func turn():
	
	# Timer used so collision boxes won't get stuck in tiles
	$TurnTimer.start()
	direction = direction * -1
	
	# Sprite offset
	if direction == 1: $spr_ape_yeti.flip_h = false
	else: $spr_ape_yeti.flip_h = true
	

# 0.2 seconds after sprite flip, collision is updated
func _on_TurnTimer_timeout():
	$Arm1.position.x *= -1
	$Arm1.rotation_degrees *= -1
	$Arm2.position.x *= -1
	$Arm2.rotation_degrees *= -1
	$Body.position.x *= -1
	$RayCast2D.position.x *= -1

# When hit with bolas, stun enemy, decrement health is stomped
func stun():
	stunned = true
	$Stun.show()
	$StunTimer.start()

# When dead, play explosion and remove collision
func dead():
	remove_child($Arm1)
	remove_child($Arm2)
	remove_child($Body)
	dieSFX.play()
	$Stun.hide()
	$DeathTimer.start()
	dead = true
	motion = Vector2(0, 0)
	$spr_ape_yeti.hide()
	$Death.show()
	state_machine.travel("Death")

func _on_StunTimer_timeout():
	stunned = false
	$Stun.hide()

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		if not dead:
			get_parent().get_node("Player").hop()
			if stunned == true:
				hitSFX.play()
				health -= 1
				$spr_ape_yeti.modulate = Color(1, 0, 0)
				$spr_ape_yeti.self_modulate = Color(1, 0, 0)
				$HitTimer.start()
			if health == 0: dead()

func _on_HitTimer_timeout():
	$spr_ape_yeti.set_modulate(Color(0.20, 0.18, 0.14))
	$spr_ape_yeti.set_self_modulate(Color(0.77, 0.60, 0.92))

func _on_DeathTimer_timeout():
	stunned = false
	if get_parent().has_node("EnemyGate"):
		get_parent().get_node("EnemyGate").activate()
	queue_free()

func _on_ChargeRange_body_entered(body):
	if body.name == "Player" and can_charge:
		$PreChargeTimer.start()
		# turn to face player
		can_charge = false
		pre_charge = true

func _on_PreChargeTimer_timeout():
	pre_charge = false
	charge = true

func _on_CooldownTimer_timeout():
	can_charge = true

func _on_HitArea_body_entered(body):
	if body.name == "Player":
		body.hit()
