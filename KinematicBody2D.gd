extends KinematicBody2D

var state_machine
var motion = Vector2()
var bolas = true
var cooldown = false
var jumping = false
var falling = false
var friction = false
var walkSFX_playing = false
var invulnerable = false
var health = 100

onready var attackSFX = get_node("AttackSFX")
onready var jumpSFX = get_node("JumpSFX")
onready var walkSFX = get_node("WalkSFX")

const UP = Vector2(0, -1)
const GRAVITY = 20
const WALK_ACCELERATION = 40
const RUN_ACCELERATION = 120
const MAX_WALK_SPEED = 200
const MAX_RUN_SPEED = 400
const JUMP_HEIGHT = -550

const BOLAS = preload("res://Bolas.tscn")

func _ready():
	var tree = $Sprite/AnimationPlayer/AnimationTree
	state_machine = tree.get("parameters/playback")

func _physics_process(delta):
	if cooldown == true:
		$BolasIcon/TextureProgress.value -= 1
		$BolasIcon/TextureProgress.tint_progress = Color(0.2, 0.2, 0.2, 0.67)
		if $BolasIcon/TextureProgress.value == 0: cooldown_end()
	else:
		$BolasIcon/TextureProgress.value = 100
		$BolasIcon/TextureProgress.tint_progress = Color(0.2, 0.2, 0.2, 0)
	
	# Apply gravity
	motion.y += GRAVITY
	
	# Basic Movements
	if Input.is_action_pressed("move_right"): moveRight()
	elif Input.is_action_pressed("move_left"): moveLeft()
	#elif Input.is_action_just_pressed("attack"): attack()
	elif Input.is_action_just_pressed("throw_bolas") and bolas == true: throwBolas()
	else: idle()
	
	if Input.is_action_just_released("move_right"): stopWalkSFX()
	if Input.is_action_just_released("move_left"): stopWalkSFX()
	
	# On-floor capabilities
	if is_on_floor():
		jumping = false
		falling = false
		if Input.is_action_just_pressed("jump"): jump()
	
	# In-air capabilities
	else:
		if walkSFX_playing: stopWalkSFX()
		friction = false
		if motion.y < 0 and not jumping:
			jumping = true
			state_machine.travel("Jump")
			#if Input.is_action_just_pressed("attack"): jumpAttack()
		elif motion.y > 0: fall()
	
	motion = move_and_slide(motion, UP, true)
	
	for i in range(get_slide_count() - 1):
		var collision = get_slide_collision(i)
		#print(collision.collider.name)
		if "Enemy" in collision.collider.name: hit()

func moveRight():
	#$CollisionShape2D.position.x = 35
	$BolasOrigin.position.x = 54
	$Sprite.flip_h = true
	motion.x = min(motion.x + WALK_ACCELERATION, MAX_WALK_SPEED)
	if not falling and not jumping: state_machine.travel("Walk")
	if not walkSFX_playing: playWalkSFX()
	#if Input.is_action_just_pressed("attack"): runAttack(false)
	if Input.is_action_pressed("sprint"): sprintRight()
	if Input.is_action_just_released("sprint"): walkSFX.pitch_scale = 1.8

func moveLeft():
	#$CollisionShape2D.position.x = 44
	$BolasOrigin.position.x = -12
	$Sprite.flip_h = false
	motion.x = max(motion.x - WALK_ACCELERATION, -MAX_WALK_SPEED)
	if not falling and not jumping: state_machine.travel("Walk")
	if not walkSFX_playing: playWalkSFX()
	#if Input.is_action_just_pressed("attack"): runAttack(false)
	if Input.is_action_pressed("sprint"): sprintLeft()
	if Input.is_action_just_released("sprint"): walkSFX.pitch_scale = 1.8

func sprintRight():
	walkSFX.pitch_scale = 2.5
	motion.x = min(motion.x + RUN_ACCELERATION, MAX_RUN_SPEED)
	if not falling and not jumping: state_machine.travel("Sprint")
	#if Input.is_action_just_pressed("attack"): runAttack(true)

func sprintLeft():
	walkSFX.pitch_scale = 2.5
	motion.x = max(motion.x - RUN_ACCELERATION, -MAX_RUN_SPEED)
	if not falling and not jumping: state_machine.travel("Sprint")
	#if Input.is_action_just_pressed("attack"): runAttack(true)

func attack():
	state_machine.travel("Attack")
	attackSFX.play()
	motion.x = lerp(motion.x, 0, 0.2)

func throwBolas():
	$BolasIcon/TextureProgress.tint_under = Color(0.51, 0.46, 0.46, 0.22)
	bolas = false
	state_machine.travel("ThrowBolas")
	$Timer.start()

func _on_Timer_timeout():
	var bolas = BOLAS.instance()
	if sign($BolasOrigin.position.x) == 1:
		bolas.set_bolas_direction(1)
	else:
		bolas.set_bolas_direction(-1)
	get_parent().add_child(bolas)
	bolas.position = $BolasOrigin.global_position

func cooldown():
	cooldown = true

func cooldown_end():
	$BolasIcon/TextureProgress.tint_under = Color(1, 1, 1, 1)
	bolas = true
	cooldown = false

func runAttack(sprinting):
	state_machine.travel("Run Attack")
	attackSFX.play()
	if sprinting: motion.x = lerp(motion.x, 0, 1.5)
	else: motion.x = lerp(motion.x, 0, 0.8)

func idle():
	friction = true
	if not falling and not jumping: state_machine.travel("Idle")
	motion.x = lerp(motion.x, 0, 0.2)
	
	# Prevents sliding on slopes
	if friction == true and get_floor_normal().y != -1:
		motion.x = 0

func jump():
	motion.y = JUMP_HEIGHT
	jumpSFX.play()

func jumpAttack():
	state_machine.travel("Jump Attack")
	attackSFX.play()

func fall():
	if not falling:
		falling = true
		$Sprite.frame = 6
	if Input.is_action_just_pressed("throw_bolas") and bolas == true: throwBolas()

func freeze():
	motion.x = 0
	motion.y = 0

func hop():
	motion.y = JUMP_HEIGHT

func playWalkSFX():
	walkSFX.play()
	walkSFX_playing = true

func stopWalkSFX():
	walkSFX.stop()
	walkSFX_playing = false

func hit():
	if $Health.frame < 6 and not invulnerable:
		global.health -= 1
		invulnerable = true
		$HitTimer.start()
		$Health.frame += 1
		$Sprite.modulate = Color(1, 0, 0)
		$Sprite.self_modulate = Color(1, 0, 0)
		if $Health.frame == 6: die()

func _on_HitTimer_timeout():
	invulnerable = false
	$Sprite.modulate = Color(1, 1, 1)
	$Sprite.self_modulate = Color(1, 1, 1)

func die():
	$Health.frame = 0
	global.health = 6
	get_tree().change_scene("res://GameOverScreen.tscn")
