extends KinematicBody2D

var state_machine
var player
var motion = Vector2()
var direction = -1
var health = 20
var stunCount = 0

var stunned = false
var dead = false
var can_charge = true
var pre_charge = false
var charge = false

onready var hitSFX = get_node("Hitmarker")
onready var dieSFX = get_node("Explosion")

const UP = Vector2(0, -1)
const GRAVITY = 20
const SPEED = 100
const JUMP = -200
const PRECHARGE = -20
const CHARGE = 450

func _ready():
	var tree = $Chungus/AnimationPlayer/AnimationTree
	state_machine = tree.get("parameters/playback")
	player = get_parent().get_node("Player")

func _physics_process(delta):
	
	# If stunned, keep enemy still on x-axis
	if pre_charge and not stunned:
		motion.x = PRECHARGE * direction
	elif charge and not stunned:
		motion.x = CHARGE * direction
	elif stunned or dead:
		motion.x = 0
	else:
		state_machine.travel("Default")
		motion.x = SPEED * direction
		if is_on_floor() == false:
			motion.y += GRAVITY
	move_and_slide(motion, UP)
	
	if is_on_wall():
		turn()
		if charge:
			charge = false
			$CooldownTimer.start()

# Turn function (adjusts all collision)
func turn():
	
	# Timer used so collision boxes won't get stuck in tiles
	$TurnTimer.start()
	direction = direction * -1
	
	# Sprite offset
	if direction == 1: $Chungus.flip_h = false
	else: $Chungus.flip_h = true

func _on_TurnTimer_timeout():
	$Body.position.x *= -1
	$Head.position.x *= -1
	$Area2D/CollisionShape2D.position.x *= -1
	$ChargeRange/CollisionShape2D.position.x *= -1
	$HitArea/CollisionShape2D.position.x *= -1
	
func stunHit():
	stunCount += 1
	player.get_node("ChungusStun").frame += 1
	if stunCount == 5:
		stun()
		stunCount = 0
		player.get_node("ChungusStun").frame = 0

# When hit with bolas, stun enemy, decrement health is stomped
func stun():
	stunned = true
	$Stun.show()
	$StunTimer.start()

# When dead, play explosion and remove collision
func dead():
	remove_child($Body)
	remove_child($Head)
	dieSFX.play()
	$Stun.hide()
	$DeathTimer.start()
	dead = true
	motion = Vector2(0, 0)
	$Chungus.hide()
	$Death.show()
	state_machine.travel("Death")

func _on_StunTimer_timeout():
	stunned = false
	$Stun.hide()

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		if not dead:
			get_parent().get_node("Player").hop()
			if stunned == true:
				hitSFX.play()
				health -= 1
				if health % 2 == 0:
					player.get_node("ChungusHP").frame += 1
				$Chungus.modulate = Color(1, 0, 0)
				$Chungus.self_modulate = Color(1, 0, 0)
				$HitTimer.start()
			if health == 0: dead()

func _on_HitTimer_timeout():
	$Chungus.set_modulate(Color(1, 1, 1))
	$Chungus.set_self_modulate(Color(1, 1, 1))

func _on_DeathTimer_timeout():
	stunned = false
	queue_free()
	get_tree().change_scene("res://WinScene.tscn")

func _on_ChargeRange_body_entered(body):
	if body.name == "Player" and can_charge:
		$PreChargeTimer.start()
		# turn to face player
		can_charge = false
		pre_charge = true

func _on_PreChargeTimer_timeout():
	pre_charge = false
	charge = true

func _on_CooldownTimer_timeout():
	can_charge = true

func _on_HitArea_body_entered(body):
	if body.name == "Player" and not dead:
		body.hit()
	elif body.name == "Hell Castle Tiles":
		turn()
