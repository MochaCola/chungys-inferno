extends Area2D

var opening = false
var gate_pos
var player
var player_pos


func _process(delta):
	if opening == true:
		player.get_node("Health").hide()
		player.get_node("BolasIcon").hide()
		player.position = gate_pos
		player.freeze()

# Activate called by lever, store values, updates player camera, waits 0.3 seconds
func activate():
	# The moment the player presses E, store positions
	gate_pos = get_parent().get_node("Gate").position
	player = get_parent().get_node("Player")
	player_pos = player.position
	$Delay.start()

# After 0.3 seconds, begin GateBottom, wait 0.4 seconds
func _on_Delay_timeout():
	opening = true
	player.get_node("Sprite").hide()
	$AnimationTimer.start()
	$GateBottom.play()
	$SmoothTimer1.start()

# After 0.4 seconds, begin GateMiddle, wait 0.4 seconds
func _on_SmoothTimer1_timeout():
	$GateMiddle.play()
	$SmoothTimer2.start()
	$StaticBody2D/CollisionShape2D.disabled = true
	$StaticBody2D.remove_child($CollisionShape2D)

# After 0.4 seconds, begin GateTop, wait for AnimationTimer
func _on_SmoothTimer2_timeout():
	$GateTop.play()

# When AnimationTimer finishes, reset player position & camera
func _on_AnimationTimer_timeout():
	opening = false
	player.position = player_pos
	player.get_node("Sprite").show()
	player.get_node("Health").show()
	player.get_node("BolasIcon").show()
